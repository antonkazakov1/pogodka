package com.kazakov.weather_mvp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by antonkazakov on 15.08.16.
 */
public class Utils {

    public static String getDateToString(long time,String sdf) {

        SimpleDateFormat sf = new SimpleDateFormat(sdf);

        Date d = new Date(time);
        return sf.format(d);
    }

    public static String getDateToString(long time) {

        SimpleDateFormat sf = new SimpleDateFormat("HH:mm");

        Date d = new Date(time);
        return sf.format(d);
    }

}

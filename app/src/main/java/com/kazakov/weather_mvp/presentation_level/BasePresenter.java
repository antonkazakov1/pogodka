package com.kazakov.weather_mvp.presentation_level;

import com.kazakov.weather_mvp.view_level.BaseView;

/**
 * Created by antonkazakov on 14.08.16.
 */
public class BasePresenter<V extends BaseView> implements Presenter<V>{

    public final String TAG = this.getClass().getSimpleName();

    public final int CODE_SUCCESS = 200;

    V baseView;

    @Override
    public void attachView(V baseView) {

        this.baseView=baseView;
    }

    @Override
    public void detachView() {

        baseView=null;
    }

    public V getBaseView(){

        return baseView;
    }

}

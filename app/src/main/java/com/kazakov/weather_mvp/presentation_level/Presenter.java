package com.kazakov.weather_mvp.presentation_level;

import com.kazakov.weather_mvp.view_level.BaseView;

/**
 * Created by antonkazakov on 14.08.16.
 */
public interface Presenter<V extends BaseView>  {

    void attachView(V baseView);

    void detachView();

}

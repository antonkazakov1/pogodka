package com.kazakov.weather_mvp.recyclerstuff;

/**
 * Created by antonkazakov on 03.07.16.
 */
public interface ButtonClick {

    void onClick(int position);

}

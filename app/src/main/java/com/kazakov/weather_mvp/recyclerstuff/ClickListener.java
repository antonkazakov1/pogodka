package com.kazakov.weather_mvp.recyclerstuff;

import android.view.View;


public interface ClickListener {

    void onClick(View view, int position);

    void onLongClick(View view, int position);

    void onDoubleClick(View view, int position);

}

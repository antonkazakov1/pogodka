package com.kazakov.weather_mvp;

/**
 * Created by antonkazakov on 14.08.16.
 */
public class Config {

    public static final String BASE_URL = "https://api.forecast.io/forecast/";

    /**
     * Dark Sky Forecast App key
     */
    public static final String APPID = "5b2b91ce25ee3fccb7c7d32f59ab4357";

}

package com.kazakov.weather_mvp.googleplaceslocations.view;

import com.kazakov.weather_mvp.view_level.BaseView;

/**
 * Created by antonkazakov on 14.08.16.
 */
public interface NewCityView extends BaseView{

    void onCityAdded();

}

package com.kazakov.weather_mvp.googleplaceslocations.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kazakov.weather_mvp.R;
import com.kazakov.weather_mvp.googleplaceslocations.model.AutoCompletePlace;

import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;


public class GooglePlacesLocationAdapter extends RecyclerView.Adapter<GooglePlacesLocationAdapter.ViewHolder> {

    private List<AutoCompletePlace> autoCompletePlaces;

    public GooglePlacesLocationAdapter(List<AutoCompletePlace> autoCompletePlaces){
        this.autoCompletePlaces = autoCompletePlaces;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.primarytv.setText(autoCompletePlaces.get(position).getPrimaryText());
        holder.secondarytv.setText(autoCompletePlaces.get(position).getSecondaryText());
    }

    @Override
    public int getItemCount() {

        return autoCompletePlaces.size();
    }

    public AutoCompletePlace getItemAtPosition(int position){

        return autoCompletePlaces.get(position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvprimary)
        TextView primarytv;

        @BindView(R.id.tvsecondary)
        TextView secondarytv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}

package com.kazakov.weather_mvp.googleplaceslocations.model;


public class AutoCompletePlace {

    private String id;
    private String primaryText,secondaryText;

    public AutoCompletePlace(){}

    public AutoCompletePlace(String id, String primaryText, String secondaryText) {
        this.id = id;
        this.primaryText=primaryText;
        this.secondaryText=secondaryText;
    }

    public String getPrimaryText() {
        return primaryText;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public void setPrimaryText(String primaryText) {
        this.primaryText = primaryText;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }
}

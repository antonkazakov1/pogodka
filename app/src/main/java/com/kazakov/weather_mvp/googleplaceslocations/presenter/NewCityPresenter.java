package com.kazakov.weather_mvp.googleplaceslocations.presenter;

import android.util.Log;

import com.kazakov.weather_mvp.presentation_level.BasePresenter;
import com.kazakov.weather_mvp.Config;
import com.kazakov.weather_mvp.api.RetrofitBuilder;
import com.kazakov.weather_mvp.weather.apicalls.CityListApi;
import com.kazakov.weather_mvp.weather.model.ForecastResponse;
import com.kazakov.weather_mvp.weather.model.TestOne;
import com.kazakov.weather_mvp.googleplaceslocations.view.NewCityView;

import io.realm.Realm;
import retrofit2.Response;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antonkazakov on 14.08.16.
 */
public class NewCityPresenter extends BasePresenter<NewCityView>{

    private Realm realm = Realm.getDefaultInstance();


    /**
     *
     * Add vc
     *
     * @param placeid google places id. I'm using this as @primarykey because its the only one unique variable i can obtain
     * @param loc
     */
    public void addCity(String placeid, String name, String loc){

        getBaseView().showProgress();

        RetrofitBuilder.createApi(CityListApi.class).getWeatherForList(Config.APPID, loc,"hourly,flags,minutely","si","ru")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ForecastResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ",e );
                        getBaseView().showError(e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(Response<ForecastResponse> forecastResponseResponse) {
                        realm.executeTransactionAsync(realm1 -> realm1.copyToRealm(new TestOne(placeid, name,forecastResponseResponse.body())),
                                () -> getBaseView().onCityAdded(),
                                (error) -> getBaseView().showError(error.getLocalizedMessage()));
                    }
                });
    }

}

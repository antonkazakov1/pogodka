package com.kazakov.weather_mvp.googleplaceslocations.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kazakov.weather_mvp.R;
import com.kazakov.weather_mvp.googleplaceslocations.model.AutoCompletePlace;
import com.kazakov.weather_mvp.googleplaceslocations.presenter.NewCityPresenter;
import com.kazakov.weather_mvp.googleplaceslocations.view.NewCityView;
import com.kazakov.weather_mvp.recyclerstuff.ClickListener;
import com.kazakov.weather_mvp.recyclerstuff.DividerItemDecoration;
import com.kazakov.weather_mvp.recyclerstuff.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


import butterknife.BindView;
import butterknife.ButterKnife;



public class GooglePlacesLocationActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        NewCityView{

    private GoogleApiClient mGoogleApiClient;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    List<AutoCompletePlace> autoCompletePlaces;

    private SearchView mSearchView;
    private MenuItem searchMenuItem;

    GooglePlacesLocationAdapter googlePlacesLocationAdapter;

    NewCityPresenter newCityPresenter;

    MaterialDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_places_activity);
        ButterKnife.bind(this);

        newCityPresenter = new NewCityPresenter();
        newCityPresenter.attachView(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        autoCompletePlaces = new ArrayList<>();
        googlePlacesLocationAdapter = new GooglePlacesLocationAdapter(autoCompletePlaces);

        initRecyclerView();

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, 0, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }

    private void initRecyclerView(){

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(googlePlacesLocationAdapter);
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (googlePlacesLocationAdapter.getItemAtPosition(position).getId() != null) {
                    findPlaceById(googlePlacesLocationAdapter.getItemAtPosition(position).getId());
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }

            @Override
            public void onDoubleClick(View view, int position) {

            }
        }));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setIconified(false);
        mSearchView.setQueryHint("Населенный пункт");
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() >= 2) {

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() >= 2) {

                    displayPredictiveResults(newText);
                }

                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    /**
     *
     * Get Google Place information using id
     *
     * @param id
     *
     */
    private void findPlaceById(String id) {

        if(TextUtils.isEmpty(id) || mGoogleApiClient == null || !mGoogleApiClient.isConnected() )
            return;
        Places.GeoDataApi.getPlaceById(mGoogleApiClient, id).setResultCallback(places -> {
            if (places.getStatus().isSuccess()) {
                newCityPresenter.addCity(id, places.get(0).getName().toString(), places.get(0).getLatLng().latitude + "," + places.get(0).getLatLng().longitude);
            }
            places.release();
        });

    }

    /**
     *
     * Display suggestions based on search query
     *
     * @param query
     *
     */
    private void displayPredictiveResults(String query) {

        autoCompletePlaces.clear();

        Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient,
                query,
                new LatLngBounds(new LatLng(39.906374, -105.122337), new LatLng(39.949552, -105.068779)),
                new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build())
                .setResultCallback(buffer -> {
                    if (buffer == null)
                        return;
                    if (buffer.getStatus().isSuccess()) {
                        for (AutocompletePrediction prediction : buffer) {

                            autoCompletePlaces.add(new AutoCompletePlace(prediction.getPlaceId(),
                                    prediction.getPrimaryText(null).toString(),
                                    prediction.getSecondaryText(null).toString()));

                        }

                        googlePlacesLocationAdapter.notifyDataSetChanged();
                    }
                    buffer.release();
                }, 60, TimeUnit.SECONDS);
    }

    @Override
    public void onConnected( Bundle bundle ) {

    }

    @Override
    public void onConnectionSuspended( int i ) {

    }

    @Override
    public void onConnectionFailed( ConnectionResult connectionResult ) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }


    @Override
    public void showError(String error) {

        if (progressDialog!=null){progressDialog.dismiss();}

        new MaterialDialog.Builder(this)
                .title("Ошибка")
                .content(error)
                .positiveText("ОК")
                .onPositive((dialog1, which) -> dialog1.dismiss())
                .show();
    }


    @Override
    public void showProgress() {

        progressDialog = new MaterialDialog.Builder(this)
                .progress(true,100)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .content("Пожалуйста подождите")
                .show();
    }

    @Override
    public void onCityAdded() {
        finish();
    }
}

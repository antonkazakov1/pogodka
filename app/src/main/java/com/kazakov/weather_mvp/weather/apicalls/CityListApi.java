package com.kazakov.weather_mvp.weather.apicalls;

import com.kazakov.weather_mvp.weather.model.ForecastResponse;


import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by antonkazakov on 14.08.16.
 */
public interface CityListApi {

    @GET("{appkey}/{location}")
    Observable<Response<ForecastResponse>> getWeatherForList(@Path("appkey") String user,
                                                             @Path("location") String location,
                                                             @Query("exclude") String exclude,
                                                             @Query("units") String units,
                                                             @Query("lang") String lang);

}

package com.kazakov.weather_mvp.weather.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by antonkazakov on 14.08.16.
 */
public class TestOne extends RealmObject{

    @PrimaryKey
    String placeId;

    String name;

    ForecastResponse forecastResponse;

    public TestOne(){}

    public TestOne(String placeId, String name,ForecastResponse forecastResponse){
        this.placeId=placeId;
        this.name=name;
        this.forecastResponse = forecastResponse;
    }


    public ForecastResponse getForecastResponse() {
        return forecastResponse;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setForecastResponse(ForecastResponse forecastResponse) {
        this.forecastResponse = forecastResponse;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.kazakov.weather_mvp.weather.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.kazakov.weather_mvp.R;
import com.kazakov.weather_mvp.googleplaceslocations.ui.GooglePlacesLocationActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CitiesListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.fab)
    public void onClickFab(){
        startActivity(new Intent(CitiesListActivity.this, GooglePlacesLocationActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
    }
}

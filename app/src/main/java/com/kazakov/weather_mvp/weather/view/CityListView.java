package com.kazakov.weather_mvp.weather.view;

import com.kazakov.weather_mvp.view_level.BaseView;
import com.kazakov.weather_mvp.weather.model.TestOne;

import io.realm.RealmResults;

/**
 * Created by antonkazakov on 14.08.16.
 */
public interface CityListView extends BaseView{

   void onRefresh(TestOne testOne);

   void onLoadList(RealmResults<TestOne> testOneRealmList);
}

package com.kazakov.weather_mvp.weather.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kazakov.weather_mvp.R;
import com.kazakov.weather_mvp.weather.view.CityListView;
import com.kazakov.weather_mvp.weather.model.TestOne;
import com.kazakov.weather_mvp.weather.presenter.CityListPresenter;
import com.kazakov.weather_mvp.recyclerstuff.ClickListener;
import com.kazakov.weather_mvp.recyclerstuff.DividerItemDecoration;
import com.kazakov.weather_mvp.recyclerstuff.RecyclerTouchListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * A placeholder fragment containing a simple view.
 */
public class CitiesListActivityFragment extends Fragment implements CityListView{

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    CitiesListAdapter citiesListAdapter;

    CityListPresenter cityListPresenter;


    MaterialDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cities_list, container, false);
        ButterKnife.bind(this, view);
        initRecyclerView();

        cityListPresenter.getCityList();

        return view;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        cityListPresenter = new CityListPresenter();
        cityListPresenter.attachView(this);

        citiesListAdapter = new CitiesListAdapter(Realm.getDefaultInstance().where(TestOne.class).findAll());

        progressDialog = new MaterialDialog.Builder(getActivity())
                .progress(true,100)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .content("Пожалуйста подождите")
                .build();


    }




    public void initRecyclerView(){

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(citiesListAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                startActivity(new Intent(getActivity(), SingleCityWeatherActivity.class)
                        .putExtra("name", citiesListAdapter.getItemAtPosition(position).getName())
                        .putExtra("placeId", citiesListAdapter.getItemAtPosition(position).getPlaceId()));
            }

            @Override
            public void onLongClick(View view, int position) {

                new MaterialDialog.Builder(getActivity())
                        .title("Удалить город")
                        .content("Вы уверены что хотите удалить город из списка?")
                        .onPositive((dialog, which) -> cityListPresenter.deleteFromRealm(citiesListAdapter.getItemAtPosition(position).getPlaceId()))
                        .positiveText("Удалить")
                        .negativeText("Отмена")
                        .onNegative((dialog, which) -> dialog.dismiss())
                        .show();
            }

            @Override
            public void onDoubleClick(View view, int position) {

            }
        }));
        swipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> cityListPresenter.getCityList());
    }


    @Override
    public void onResume() {
        super.onResume();
        citiesListAdapter.notifyDataSetChanged();
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        cityListPresenter.detachView();
    }



    @Override
    public void showError(String error) {

        new MaterialDialog.Builder(getActivity())
                .title("Ошибка")
                .content(error)
                .positiveText("ОК")
                .onPositive((dialog1, which) -> dialog1.dismiss())
                .show();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(false);
       progressDialog.show();
    }

    public CitiesListActivityFragment() {}

    @Override
    public void onRefresh(TestOne testOne) {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
        swipeRefreshLayout.setRefreshing(false);
        citiesListAdapter.notifyDataSetChanged();
    }



    @Override
    public void onLoadList(RealmResults<TestOne> testOneRealmList) {

        citiesListAdapter.notifyDataSetChanged();
    }
}

package com.kazakov.weather_mvp.weather.ui;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kazakov.weather_mvp.AppSingleton;
import com.kazakov.weather_mvp.R;
import com.kazakov.weather_mvp.weather.view.CityListView;
import com.kazakov.weather_mvp.weather.model.TestOne;
import com.kazakov.weather_mvp.weather.presenter.CityListPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;

public class SingleCityWeatherActivity extends AppCompatActivity implements CityListView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_icon)
    ImageView img_icon;

    @BindView(R.id.tv_temperature)
    TextView tv_temperature;

    @BindView(R.id.tv_wind)
            TextView tv_wind;

    @BindView(R.id.tv_pressure)
            TextView tv_pressure;

    @BindView(R.id.tv_cloud_cover)
    TextView tv_cloud_cover;

    CityListPresenter cityListPresenter;

    DailyWeatherAdapter dailyWeatherAdapter;

    private static final String CLEAR_DAY = "clear-day";
    private static final String CLEAR_NIGHT = "clear-night";
    private static final String RAIN = "rain";
    private static final String SNOW = "snow";
    private static final String SLEET = "sleet";
    private static final String WIND = "wind";
    private static final String FOG = "fog";
    private static final String CLOUDY = "cloudy";
    private static final String PARTLY_CLOUDY_DAY = "partly-cloudy-day";
    private static final String PARTLY_CLOUDY_NIGHT = "partly-cloudy-night";


    MaterialDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivty_single_weather);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra("name"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitleTextColor(ContextCompat.getColor(this,R.color.white));
        toolbar.setNavigationIcon(R.drawable.toolbar_close);

        cityListPresenter = new CityListPresenter();
        cityListPresenter.attachView(this);
        cityListPresenter.getWeatherFromRealm(getIntent().getStringExtra("placeId"));

    }


    @Override
    public void onRefresh(TestOne testOne) {
        tv_cloud_cover.setText("Облачность: " + testOne.getForecastResponse().getCurrently().getCloudCover() + " %");
        tv_temperature.setText(testOne.getForecastResponse().getCurrently().getTemperature() + "\u00b0");
        tv_pressure.setText("Давление: " + testOne.getForecastResponse().getCurrently().getPressure());
        tv_wind.setText("Ветер: " + testOne.getForecastResponse().getCurrently().getWindSpeed() + " м/c");
        switch (testOne.getForecastResponse().getCurrently().getIcon()){
            case CLEAR_DAY:
                img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.clear_day_48_10));
                break;
            case CLEAR_NIGHT:
                img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.clear_night_48_10));
                break;
            case RAIN:
               img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.rainy_48_10));
                break;
            case SNOW:
                img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.snow_48_10));
                break;
            case WIND:
                img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.windy_48_10));
                break;
            case FOG:
              img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.windy_48_10));
                break;
            case CLOUDY:
                img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.cloudy_48_10));
                break;
            case PARTLY_CLOUDY_DAY:
                img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.partly_cloudy_48_10));
                break;
            case PARTLY_CLOUDY_NIGHT:
                img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.cloudy_night_48_10));
                break;

        }


    }

    @Override
    public void onLoadList(RealmResults<TestOne> testOneRealmList) {

    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void showProgress() {

        progressDialog = new MaterialDialog.Builder(this)
                .progress(true,100)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .content("Пожалуйста подождите")
                .show();
    }

    private void fillInfo(){

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


}

package com.kazakov.weather_mvp.weather.ui;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kazakov.weather_mvp.AppSingleton;
import com.kazakov.weather_mvp.R;
import com.kazakov.weather_mvp.utils.Utils;
import com.kazakov.weather_mvp.weather.model.Datum;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmChangeListener;
import io.realm.RealmList;

/**
 * Created by antonkazakov on 14.08.16.
 */
public class DailyWeatherAdapter extends RecyclerView.Adapter<DailyWeatherAdapter.CitiesListViewHolder> implements RealmChangeListener{

    private static final String CLEAR_DAY = "clear-day";
    private static final String CLEAR_NIGHT = "clear-night";
    private static final String RAIN = "rain";
    private static final String SNOW = "snow";
    private static final String SLEET = "sleet";
    private static final String WIND = "wind";
    private static final String FOG = "fog";
    private static final String CLOUDY = "cloudy";
    private static final String PARTLY_CLOUDY_DAY = "partly-cloudy-day";
    private static final String PARTLY_CLOUDY_NIGHT = "partly-cloudy-night";

    private RealmList<Datum> weatherItemRealmResults;

    public DailyWeatherAdapter(RealmList<Datum> weatherItemRealmResults){

        this.weatherItemRealmResults=weatherItemRealmResults;
    }


    @Override
    public CitiesListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new CitiesListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_daily_item,parent,false));
    }

    @Override
    public void onBindViewHolder(CitiesListViewHolder holder, int position) {

        holder.tv_dayname.setText(Utils.getDateToString(weatherItemRealmResults.get(position).getTime()*1000L,"EEEE"));
        holder.tv_temp.setText(Math.round(weatherItemRealmResults.get(position).getTemperatureMin())+ "..." +Math.round(weatherItemRealmResults.get(position).getTemperatureMax()) + "\u00b0");
        switch (weatherItemRealmResults.get(position).getIcon()){
            case CLEAR_DAY:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.clear_day_48_10));
                break;
            case CLEAR_NIGHT:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.clear_night_48_10));
                break;
            case RAIN:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.rainy_48_10));
                break;
            case SNOW:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.snow_48_10));
                break;
            case WIND:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.windy_48_10));
                break;
            case FOG:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.windy_48_10));
                break;
            case CLOUDY:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.cloudy_48_10));
                break;
            case PARTLY_CLOUDY_DAY:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.partly_cloudy_48_10));
                break;
            case PARTLY_CLOUDY_NIGHT:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.cloudy_night_48_10));
                break;
            default:
                holder.img_icon.setImageDrawable(ContextCompat.getDrawable(AppSingleton.getContext(),R.drawable.weather_unknown_48_10));
                break;
        }

    }

    @Override
    public int getItemCount() {

        return weatherItemRealmResults.size();
    }

    @Override
    public void onChange(Object element) {

        notifyDataSetChanged();
    }


    public Datum getItemAtPosition(int position){

        return weatherItemRealmResults.get(position);
    }


    class CitiesListViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_dayname)
        TextView tv_dayname;

        @BindView(R.id.tv_temp)
        TextView tv_temp;

        @BindView(R.id.img_icon)
        ImageView img_icon;

        public CitiesListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}

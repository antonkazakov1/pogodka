package com.kazakov.weather_mvp.weather.presenter;

import android.util.Log;

import com.kazakov.weather_mvp.presentation_level.BasePresenter;
import com.kazakov.weather_mvp.Config;
import com.kazakov.weather_mvp.weather.model.ForecastResponse;
import com.kazakov.weather_mvp.weather.model.TestOne;
import com.kazakov.weather_mvp.api.RetrofitBuilder;
import com.kazakov.weather_mvp.weather.view.CityListView;
import com.kazakov.weather_mvp.weather.apicalls.CityListApi;



import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import retrofit2.Response;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antonkazakov on 14.08.16.
 */
public class CityListPresenter extends BasePresenter<CityListView>{

    private Realm realm = Realm.getDefaultInstance();

    /**
     *
     * Get cities list
     *
     */
    public void getCityList(){

        if (realm.where(TestOne.class).findAll().size()!=0){
            getBaseView().showProgress();
        for (TestOne testone : realm.where(TestOne.class).findAll()){
            RetrofitBuilder.createApi(CityListApi.class).getWeatherForList(Config.APPID,testone.getForecastResponse().getLatitude() + "," + testone.getForecastResponse().getLongitude(),"hourly,flags,minutely","si","ru")
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ForecastResponse>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "onError: ", e);
                        }

                        @Override
                        public void onNext(Response<ForecastResponse> forecastResponseResponse) {
                            insertIntoRealm(new TestOne(testone.getPlaceId(), testone.getName(), forecastResponseResponse.body()));
                        }
                    });
        }}

    }


    /**
     *
     * Get weatherlist from Realm
     *
     */
    public void getWeatherList(){

        realm.where(TestOne.class).findAll().asObservable()
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RealmResults<TestOne>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ", e);
                    }

                    @Override
                    public void onNext(RealmResults<TestOne> testOnes) {
                        Log.i("onError: ", "dsfsdfs");
                        getBaseView().onLoadList(testOnes);
                    }
                });

    }


    /**
     *
     * Get Single weather item from realm for city with @placeId
     *
     * @param placeId
     * @return
     */
    public void getWeatherFromRealm(String placeId){

      realm.where(TestOne.class).equalTo("placeId", placeId).findFirst().asObservable()
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RealmObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ", e);
                    }

                    @Override
                    public void onNext(RealmObject realmObject) {
                        getBaseView().onRefresh(((TestOne)realmObject));
                    }
                });
    }


    /**
     *
     * Insert single to realm
     *
     * @param testOne
     */
    private void insertIntoRealm(TestOne testOne){

        realm.executeTransactionAsync(realm1 -> realm1.copyToRealmOrUpdate(new TestOne(testOne.getPlaceId(),testOne.getName(), testOne.getForecastResponse())),
                () -> getBaseView().onRefresh(testOne),
                error -> Log.e(TAG, "onError: ",error ));
    }


    /**
     *
     * Delete from realm by placeId
     *
     * @param placeId
     */
    public void deleteFromRealm(String placeId){

        realm.executeTransactionAsync(realm1 -> realm1.where(TestOne.class).equalTo("placeId", placeId).findFirst().deleteFromRealm(),
                () -> {},
                error -> Log.e(TAG, "onError: ", error));
    }



}
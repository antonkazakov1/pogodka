package com.kazakov.weather_mvp.api;

import com.kazakov.weather_mvp.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by antonkazakov on 14.08.16.
 */
public class RetrofitBuilder {

    /**
     *
     * OkHttpClient to pass it into retrofit with logging interceptor
     *
     */
    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)) //Logging interceptor
            .addNetworkInterceptor(chain -> {
                Request request = chain.request().newBuilder()
                        .method(chain.request().method(), chain.request().body())
                        .build();
                return chain.proceed(request);
            })

            .build();


    /**
     *
     * Retrofit client builder
     *
     */
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Config.BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();


    /**
     *
     * Generic T to return retrofit instance to specific tClass
     *
     * @param tClass
     * @param <T>
     * @return
     *
     */
    public static <T> T createApi(Class<T> tClass){
        return retrofit.create(tClass);
    }
}

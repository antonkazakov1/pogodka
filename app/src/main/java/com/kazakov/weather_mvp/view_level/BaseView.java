package com.kazakov.weather_mvp.view_level;

/**
 * Created by antonkazakov on 14.08.16.
 */
public interface BaseView {

    void showError(String error);

    void showProgress();

}

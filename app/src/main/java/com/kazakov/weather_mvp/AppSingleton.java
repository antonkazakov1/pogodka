package com.kazakov.weather_mvp;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by antonkazakov on 14.08.16.
 */
public class AppSingleton extends Application{

    public static AppSingleton applicationSingleton;

    public AppSingleton() {

    }

    public static AppSingleton getContext() {
        return applicationSingleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationSingleton=this;

        initRealm();
    }


    /**
     *
     * Init realm
     *
     */
    private void initRealm(){
        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext())
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(6)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

}
